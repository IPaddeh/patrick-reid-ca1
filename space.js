var Spaceface =  function () {
   this.canvas = document.getElementById('game-canvas'),
   this.context = this.canvas.getContext('2d'),

   // ********************** HTML elements ********************** 
   
   this.fpsElement = document.getElementById('fps'),
   this.toast = document.getElementById('toast'),
   this.scoreElement = document.getElementById('score'),
   this.levelElement = document.getElementById('level'),

   // ********************** Constants ********************** 

   this.LEFT = 1,
   this.RIGHT = 2,
   this.level = 1;
   this.speed = 20;
         
   this.BACKGROUND_VELOCITY = 38,    // pixels/second
   this.SPACE_ANIMATION_RATE = 35,     // frames/second
   
   this.ROCK_SPARKLE_DURATION = 100, // milliseconds
   this.ROCK_SPARKLE_INTERVAL = 300, // milliseconds
   this.ROCK_BOUNCE_RISE_DURATION = 80, // milliseconds
   

   this.PAUSED_CHECK_INTERVAL = 200,
   this.DEFAULT_TOAST_TIME = 3000, // 3 seconds

   this.EXPLOSION_CELLS_HEIGHT = 55,
   this.EXPLOSION_DURATION = 500,


   this.PLATFORM_HEIGHT = 8,  
   this.PLATFORM_STROKE_WIDTH = 2,
   this.PLATFORM_STROKE_STYLE = 'rgb(0,0,0)',

   this.SATALITE_CELLS_WIDTH = 135,
   this.SATALITE_CELLS_HEIGHT = 76,
   this.PLANETONE_CELLS_WIDTH = 118,
   this.PLANETONE_CELLS_HEIGHT = 118,
   this.TWO_CELLS_HEIGHT = 188,
   this.TWO_CELLS_WIDTH = 188,
   this.THREE_CELLS_HEIGHT = 210,
   this.THREE_CELLS_WIDTH = 210,
   this.FOUR_CELLS_HEIGHT = 142,
   this.FOUR_CELLS_WIDTH = 142,
   this.SPACEROCK_CELLS_HEIGHT = 145,
   this.SPACEROCK_CELLS_WIDTH = 120,
   this.SPACE_CELLS_HEIGHT = 52,
   
   
   // times as fast as the background.

   this.PLATFORM_VELOCITY_MULTIPLIER = 4.35,

   this.SPACESHIP_CELLS_WIDTH = 40, // pixels
   this.SPACESHIP_CELLS_HEIGHT = 52, // pixels

   this.SPACESHIP_HEIGHT = 43,
   this.RSPACESHIP_JUMP_HEIGHT = 120,

   this.SPACE_ANIMATION_INITIAL_RATE = 0,

  

   this.BEE_CELLS_HEIGHT = 50,
   this.BEE_CELLS_WIDTH  = 50,

   this.INITIAL_BACKGROUND_VELOCITY = 0,
   this.INITIAL_BACKGROUND_OFFSET = 0,
   this.INITIAL_SPACESHIP_LEFT = 50,
   this.INITIAL_SPACESHIP_TRACK = 1,
   this.INITIAL_SPACESHIP_VELOCITY = 0,

   // ********************** Paused ********************** 
   
   this.paused = false,
   this.pauseStartTime = 0,
   this.totalTimePaused = 0,

   this.windowHasFocus = true,

   // ********************** Track baselines ********************** 

   this.TRACK_0_BASELINE = 372,
   this.TRACK_1_BASELINE = 348,
   this.TRACK_2_BASELINE = 323,
   this.TRACK_3_BASELINE = 298,
   this.TRACK_4_BASELINE = 272,
   this.TRACK_5_BASELINE = 248,
   this.TRACK_6_BASELINE = 223,
   this.TRACK_7_BASELINE = 198,
   this.TRACK_8_BASELINE = 173,
   this.TRACK_9_BASELINE = 148,
   this.TRACK_10_BASELINE = 123,
   this.TRACK_11_BASELINE = 98,
   this.TRACK_12_BASELINE = 72,

   // ********************** Fps indicator ********************** 
   
   this.fpsToast = document.getElementById('fps'),

   // ********************** Images **********************
   
   this.background  = new Image(),
   this.spritesheet = new Image(),

  // ********************** Time ********************** 
   
   this.lastAnimationFrameTime = 0,
   this.lastFpsUpdateTime = 0,
   this.fps = 60,

   // ********************** Translation offsets **********************
   
   this.backgroundOffset = this.INITIAL_BACKGROUND_OFFSET,
   this.spriteOffset = this.INITIAL_BACKGROUND_OFFSET,

   // ********************** Velocities ********************** 

   this.bgVelocity = this.INITIAL_BACKGROUND_VELOCITY,
   this.platformVelocity,

   // ********************** Spaceship ********************** 

   this.spaceCellsRight = [
      { left: 18, top: 11, width: 110, height: this.SPACE_CELLS_HEIGHT },
      { left: 150, top: 11, width: 110, height: this.SPACE_CELLS_HEIGHT },
      { left: 284, top: 11, width: 110, height: this.SPACE_CELLS_HEIGHT }

   ],

// ********************** Satalite ********************** 


   this.sataliteData = [
       
      //Level One
      {left: 400, top: this.TRACK_3_BASELINE},
      {left: 400, top: this.TRACK_8_BASELINE},
      {left: 1200, top: this.TRACK_10_BASELINE},
      {left: 1200, top: this.TRACK_4_BASELINE},
      {left: 1800, top: this.TRACK_12_BASELINE},
      {left: 1800, top: this.TRACK_2_BASELINE},
      {left: 2400, top: this.TRACK_5_BASELINE},
      {left: 2400, top: this.TRACK_9_BASELINE},
      {left: 2900, top: this.TRACK_2_BASELINE},
      {left: 2900, top: this.TRACK_7_BASELINE},
      {left: 3500, top: this.TRACK_12_BASELINE},
      {left: 3500, top: this.TRACK_8_BASELINE},
      
      //Level Two
      {left: 4000, top: this.TRACK_3_BASELINE},
      {left: 4000, top: this.TRACK_8_BASELINE},
      {left: 4800, top: this.TRACK_10_BASELINE},
      {left: 4800, top: this.TRACK_4_BASELINE},
      {left: 5400, top: this.TRACK_12_BASELINE},
      {left: 5400, top: this.TRACK_2_BASELINE},
      {left: 6000, top: this.TRACK_5_BASELINE},
      {left: 6000, top: this.TRACK_9_BASELINE},
      {left: 6700, top: this.TRACK_2_BASELINE},
      {left: 6700, top: this.TRACK_7_BASELINE},
      {left: 7500, top: this.TRACK_12_BASELINE},
      {left: 7500, top: this.TRACK_8_BASELINE},
      
      //Level Three
      {left: 8000, top: this.TRACK_3_BASELINE},
      {left: 8000, top: this.TRACK_8_BASELINE},
      {left: 8800, top: this.TRACK_10_BASELINE},
      {left: 8800, top: this.TRACK_4_BASELINE},
      {left: 9400, top: this.TRACK_12_BASELINE},
      {left: 9400, top: this.TRACK_2_BASELINE},
      {left: 10000, top: this.TRACK_5_BASELINE},
      {left: 10000, top: this.TRACK_9_BASELINE},
      {left: 10700, top: this.TRACK_2_BASELINE},
      {left: 10700, top: this.TRACK_7_BASELINE},
      {left: 11500, top: this.TRACK_12_BASELINE},
      {left: 11500, top: this.TRACK_8_BASELINE},
      
      //Level Four
      {left: 13000, top: this.TRACK_3_BASELINE},
      {left: 13000, top: this.TRACK_8_BASELINE},
      {left: 13800, top: this.TRACK_10_BASELINE},
      {left: 13800, top: this.TRACK_4_BASELINE},
      {left: 14400, top: this.TRACK_12_BASELINE},
      {left: 14400, top: this.TRACK_2_BASELINE},
      {left: 15000, top: this.TRACK_5_BASELINE},
      {left: 15000, top: this.TRACK_9_BASELINE},
      {left: 16700, top: this.TRACK_2_BASELINE},
      {left: 16700, top: this.TRACK_7_BASELINE},
      {left: 17500, top: this.TRACK_12_BASELINE},
      {left: 17500, top: this.TRACK_8_BASELINE},
       
      //Level Five
      {left: 19000, top: this.TRACK_3_BASELINE},
      {left: 19000, top: this.TRACK_8_BASELINE},
      {left: 19800, top: this.TRACK_10_BASELINE},
      {left: 19800, top: this.TRACK_4_BASELINE},
      {left: 20400, top: this.TRACK_12_BASELINE},
      {left: 20400, top: this.TRACK_2_BASELINE},
      {left: 21000, top: this.TRACK_5_BASELINE},
      {left: 21000, top: this.TRACK_9_BASELINE},
      {left: 21600, top: this.TRACK_2_BASELINE},
      {left: 21600, top: this.TRACK_7_BASELINE},
      {left: 22500, top: this.TRACK_12_BASELINE},
      {left: 22000, top: this.TRACK_8_BASELINE}
      
   ],
        
    //  ********************** SpaceRock ********************** 
            
   this.rockData = [
       //Level One
      { left: 850,  top: this.TRACK_9_BASELINE},
      { left: 1400,  top: this.TRACK_12_BASELINE},
      { left: 2100,  top: this.TRACK_12_BASELINE},
      { left: 2600,  top: this.TRACK_5_BASELINE},
      
       //Level two
      { left: 4400,  top: this.TRACK_9_BASELINE},
      { left: 5500,  top: this.TRACK_5_BASELINE},
      { left: 6600,  top: this.TRACK_5_BASELINE},
      
       //Level Three
      { left: 8400,  top: this.TRACK_9_BASELINE},
      { left: 10100,  top: this.TRACK_12_BASELINE},
      { left: 10650,  top: this.TRACK_5_BASELINE},
      
      
       //Level Four
      { left: 13400,  top: this.TRACK_9_BASELINE},
      { left: 15100,  top: this.TRACK_12_BASELINE},
      { left: 15650,  top: this.TRACK_5_BASELINE},
      
      
       //Level Five
      { left: 19400,  top: this.TRACK_9_BASELINE},
      { left: 21050,  top: this.TRACK_12_BASELINE},
      { left: 21600,  top: this.TRACK_5_BASELINE}
      

      
   ],


// ********************** Planet Two ********************** 
   this.twoData = [
       {left: 7750, top: this.TRACK_2_BASELINE}
   ],

   // ********************** Planet One ********************** 

   this.pOneData = [
      {left: 3750, top: this.TRACK_7_BASELINE}
   ],

   // ********************** Planet Three ********************** 

   this.threeData = [
      {left: 12200, top: this.TRACK_12_BASELINE}
   ],

   // ********************** Planet Four ********************** 

   this.fourData = [
      { left: 18200,  top: this.TRACK_11_BASELINE}
   ],

 // ********************** Spritesheet cells ********************** 
   
   this.sataliteCells = [
	{ left: 16, top: 304, width: this.SATALITE_CELLS_WIDTH,height: this.SATALITE_CELLS_HEIGHT }
   ],
           
   this.rockCells = [
      { left: 20, top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 138,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 272,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 415,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 565,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 702,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT }
   ],
            
    this.twoCells = [
      {left: 150, top: 83, width:  this.TWO_CELLS_HEIGHT, height: this.TWO_CELLS_WIDTH}
   ],

   this.pOneCells = [
      {left: 16, top: 115, width: this.PLANETONE_CELLS_WIDTH, height: this.PLANETONE_CELLS_HEIGHT}
   ],
           
    this.threeCells = [
      {left: 356, top: 76, width: this.THREE_CELLS_WIDTH, height: this.THREE_CELLS_HEIGHT}
   ],
           
    this.fourCells = [
      { left: 591,   top: 102, width: this.FOUR_CELLS_WIDTH, height: this.FOUR_CELLS_HEIGHT}
   ],


 this.explosionCells = [
      { left: 185, top: 324, width: 50, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 255, top: 312, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 343, top: 315, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 424, top: 314, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 502, top: 316, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 586, top: 309, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 666, top: 310, width: 68, height: this.EXPLOSION_CELLS_HEIGHT }
   ],
  
   this.rockCells = [
      { left: 20, top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 138,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 272,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 415,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 565,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT },
      { left: 702,top: 419, width: this.SPACEROCK_CELLS_WIDTH,height: this.SPACEROCK_CELLS_HEIGHT }
   ],

 
   this.satalites    = [], 
   this.pTwos        = [],
   this.pOne         = [],
   this.pThree       = [],
   this.pFour        = [],
   this.rocks        = [],
   this.platforms    = [],

   
  // ********************** Sprite artists ********************** 

   this.spaceArtist = new SpriteSheetArtist(this.spritesheet,
                                             this.spaceCellsRight),

   this.platformArtist = {
      draw: function (sprite, context) {
         var top;
         
         context.save();

         top = spaceface.calculatePlatformTop(sprite.track);

         context.lineWidth = spaceface.PLATFORM_STROKE_WIDTH;
         context.strokeStyle = spaceface.PLATFORM_STROKE_STYLE;
         context.fillStyle = sprite.fillStyle;

         context.strokeRect(sprite.left, top, sprite.width, sprite.height);
         context.fillRect  (sprite.left, top, sprite.width, sprite.height);

         context.restore();
      }
   },

    // ********************** Sprite behaviors ********************** 

   this.spaceBehavior = {

      lastAdvanceTime: 0,
      
      execute: function(sprite, time, fps) {
         if (sprite.spaceAnimationRate === 0) {
            return;
         }
         
         if (this.lastAdvanceTime === 0) {  // skip first time
            this.lastAdvanceTime = time;
         }
         else if (time - this.lastAdvanceTime > 1000 / sprite.spaceAnimationRate) {
            sprite.artist.advance();
            this.lastAdvanceTime = time;
         }
      }
   },


     this.jumpBehavior = {
      execute: function(sprite, time, fps) {
         if (sprite.jumping) {
            if (sprite.track !== 12) {
               sprite.track++;
            }
            sprite.top = spaceface.calculatePlatformTop(sprite.track) -
                         spaceface.SPACESHIP_CELLS_HEIGHT;

            sprite.jumping = false; // immediately done jumping for now
         }
      } 
   },
           
    this.fallBehavior = {
      execute: function(sprite, time, fps) {
         if (sprite.falling) {
            if (sprite.track !== 0) {
               sprite.track--;
               
               sprite.top = spaceface.calculatePlatformTop(sprite.track) -
                         spaceface.SPACESHIP_CELLS_HEIGHT;
           }
            sprite.falling = false; // immediately done falling for now
         } 
      } 
   }, 

   

   this.collideBehavior = {
      execute: function (sprite, time, fps, context) {
         var otherSprite;

         for (var i=0; i < spaceface.sprites.length; ++i) { 
            otherSprite = spaceface.sprites[i];

            if (this.isCandidateForCollision(sprite, otherSprite)) {
               if (this.didCollide(sprite, otherSprite, context)) { 
                  this.processCollision(sprite, otherSprite);
               }
            }
         }
      },

      isCandidateForCollision: function (sprite, otherSprite) {
         return sprite !== otherSprite &&
                sprite.visible && otherSprite.visible &&
                !sprite.exploding && !otherSprite.exploding &&
                otherSprite.left - otherSprite.offset <
                   sprite.left - sprite.offset + sprite.width;
      }, 

      

      didSpaceCollideWithOtherSprite: function (left, top, right, bottom,
                                                 centerX, centerY,
                                                 otherSprite, context) {
        
         context.beginPath();
         context.rect(otherSprite.left - otherSprite.offset, otherSprite.top,
                      otherSprite.width, otherSprite.height);
         
         return context.isPointInPath(left,    top)     ||
                context.isPointInPath(right,   top)     ||

                context.isPointInPath(centerX, centerY) ||

                context.isPointInPath(left,    bottom)  ||
                context.isPointInPath(right,   bottom);
      },
     
      didCollide: function (sprite, otherSprite, context) {
         var MARGIN_TOP = 5,
             MARGIN_LEFT = 5,
             MARGIN_RIGHT = 5,
             MARGIN_BOTTOM = 0,
             left = sprite.left + sprite.offset + MARGIN_LEFT,
             right = sprite.left + sprite.offset + sprite.width - MARGIN_RIGHT,
             top = sprite.top + MARGIN_TOP,
             bottom = sprite.top + sprite.height - MARGIN_BOTTOM,
             centerX = left + sprite.width/2,
             centerY = sprite.top + sprite.height/2;


            return this.didSpaceCollideWithOtherSprite(left, top, right, bottom,
                                                  centerX, centerY,
                                                  otherSprite, context);
         
      },

      processCollision: function (sprite, otherSprite) {
         if (otherSprite.value) {
             this.SCORE = this.SCORE + 1;
             this.scoreElement.innerHTML = this.score;
         }

       

         if ('rock' === otherSprite.type   ||
             'one' === otherSprite.type   ||
             'two' === otherSprite.type   ||
             'three' === otherSprite.type   ||
             'four' === otherSprite.type   ||
             'satalite' === otherSprite.type) {
            spaceface.explode(sprite);
            this.SCORE = 0;
         }

         if (sprite.jumping && 'platform' === otherSprite.type) {
            this.processPlatformCollisionDuringJump(sprite, otherSprite);
         }
      },

      processPlatformCollisionDuringJump: function (sprite, platform) {
         var isDescending = sprite.descendAnimationTimer.isRunning();

         sprite.stopJumping();

         if (isDescending) { // Collided with platform while descending
            // land on platform
            sprite.track = platform.track; 
            sprite.top = spaceface.calculatePlatformTop(sprite.track) - sprite.height;
         }
         else { // Collided with platform while ascending
            sprite.fall();
         }
      }
   };

// ********************** General pace behavior ********************** 
   

   this.paceBehavior = {
      execute: function (sprite, time, fps) {
         var sRight = sprite.left + sprite.width,
             pRight = sprite.platform.left + sprite.platform.width,
             pixelsToMove = sprite.velocityX / fps;

         if (sprite.direction === undefined) {
            sprite.direction = spaceface.RIGHT;
         }

         if (sRight > pRight && sprite.direction === spaceface.RIGHT) {
            sprite.direction = spaceface.LEFT;
         }
         else if (sprite.left < sprite.platform.left &&
                  sprite.direction === spaceface.LEFT) {
            sprite.direction = spaceface.RIGHT;
         }

         if (sprite.direction === spaceface.RIGHT) {
            sprite.left += pixelsToMove;
         }
         else {
            sprite.left -= pixelsToMove;
         }
      }
   };
   
   // ********************** Sprites ********************** 

   this.space = new Sprite('space',           // type
                            this.spaceArtist,  // artist
                            [ this.spaceBehavior, // behaviors
                              this.jumpBehavior,
                              this.fallBehavior,
                              this.collideBehavior
                            ]); 

   this.space.width = this.SPACESHIP_CELLS_WIDTH;
   this.space.height = this.SPACESHIP_CELLS_HEIGHT;

   this.sprites = [ this.space ];  

   this.explosionAnimator = new SpriteAnimator(
      this.explosionCells,          // Animation cells
      this.EXPLOSION_DURATION,      // Duration of the explosion

      function (sprite, animator) { // Callback after animation
         sprite.exploding = false; 

         if (sprite.jumping) {
            sprite.stopJumping();
         }
       

         sprite.visible = true;
         sprite.track = 1;
         sprite.top = spaceface.calculatePlatformTop(sprite.track) - sprite.height;
         sprite.artist.cellIndex = 0;
         sprite.spaceAnimationRate = spaceface.SPACE_ANIMATION_RATE;
      }
   );
};



Spaceface.prototype = {
   // ********************** Drawing ********************** 

   draw: function (now) {
      this.setPlatformVelocity();
      this.setTranslationOffsets();

      this.drawBackground();

      this.updateSprites(now);
      this.drawSprites();
   },

   setPlatformVelocity: function () {
      this.platformVelocity = this.bgVelocity * this.PLATFORM_VELOCITY_MULTIPLIER; 
   },

   setTranslationOffsets: function () {
      this.setBackgroundTranslationOffset();
      this.setSpriteTranslationOffsets();
   },
   
   setSpriteTranslationOffsets: function () {
      var i, sprite;
   
      this.spriteOffset += this.platformVelocity / this.fps; // In step with platforms

      for (i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];
      
         if ('space' !== sprite.type) {
            sprite.offset = this.spriteOffset; 
         }
      }
   },

   setBackgroundTranslationOffset: function () {
      var offset = this.backgroundOffset + this.bgVelocity/this.fps;
   
      if (offset > 0 && offset < this.background.width) {
         this.backgroundOffset = offset;
      }
      else {
         this.backgroundOffset = 0;
      }
   },
   
   drawBackground: function () {
      this.context.save();
   
      this.context.globalAlpha = 1.0;
      this.context.translate(-this.backgroundOffset, 0);
   
      // Initially onscreen:
      this.context.drawImage(this.background, 0, 0,
                        this.background.width, this.background.height);
   
      // Initially offscreen:
      this.context.drawImage(this.background, this.background.width, 0,
                        this.background.width+1, this.background.height);
   
      this.context.restore();
   },
   
   calculateFps: function (now) {
      var fps = 1000 / (now - this.lastAnimationFrameTime);
      var score = 0;
      this.lastAnimationFrameTime = now;
   
      if (now - this.lastFpsUpdateTime > 1000) {
         this.lastFpsUpdateTime = now;
         this.fpsElement.innerHTML = fps.toFixed(0) + ' fps';
         score = now / 1000;
         this.scoreElement.innerHTML = score.toFixed(0);
         this.levelElement.innerHTML = 'Level: ' + this.level;
         
         if(score >= this.speed)
         {
             this.bgVelocity = this.bgVelocity + 10;
             this.speed = this.speed + 20;
             this.level = this.level + 1;
         }
      }

      return fps; 
   },
   
   calculatePlatformTop: function (track) {
      var top;
   
      if      (track === 0) { top = this.TRACK_0_BASELINE; }
      else if (track === 1) { top = this.TRACK_1_BASELINE; }
      else if (track === 2) { top = this.TRACK_2_BASELINE; }
      else if (track === 3) { top = this.TRACK_3_BASELINE; }
      else if (track === 4) { top = this.TRACK_4_BASELINE; }
      else if (track === 5) { top = this.TRACK_5_BASELINE; }
      else if (track === 6) { top = this.TRACK_6_BASELINE; }
      else if (track === 7) { top = this.TRACK_7_BASELINE; }
      else if (track === 8) { top = this.TRACK_8_BASELINE; }
      else if (track === 9) { top = this.TRACK_9_BASELINE; }
      else if (track === 10) { top = this.TRACK_10_BASELINE; }
      else if (track === 11) { top = this.TRACK_11_BASELINE; }
      else if (track === 12) { top = this.TRACK_12_BASELINE; }

      return top;
   },

   turnLeft: function () {
      this.bgVelocity = -this.BACKGROUND_VELOCITY;
      this.space.spaceAnimationRate = this.SPACE_ANIMATION_RATE;
      this.space.direction = this.LEFT;
   },

   turnRight: function () {
      this.bgVelocity = this.BACKGROUND_VELOCITY;
      this.space.spaceAnimationRate = this.SPACE_ANIMATION_RATE;
      this.space.direction = this.RIGHT;
   },
   
   // ********************** Sprites ********************** 

   equipSpace: function () {
      // Animation rate, track, direction, velocity,
      // position, and artist cells........................................
      
      this.space.spaceAnimationRate = this.SPACE_ANIMATION_INITIAL_RATE,
   
      this.space.track = this.INITIAL_SPACESHIP_TRACK;
      this.space.direction = this.LEFT;
      this.space.velocityX = this.INITIAL_SPACESHIP_VELOCITY;
      this.space.left = this.INITIAL_SPACESHIP_LEFT;
      this.space.top = this.calculatePlatformTop(this.space.track) -
                        this.SPACESHIP_CELLS_HEIGHT;

      this.space.artist.cells = this.spaceCellsRight;
      this.space.offset = 0;
      
      this.space.jumping = false;
      this.space.falling = false;

       this.space.jump = function () {

         this.jumping = !this.jumping
      };

      this.space.fall = function () {


         this.falling = !this.falling
      };

   },

 
   explode: function (sprite, silent) {
      if (sprite.spaceAnimationRate === 0) {
         sprite.spaceAnimationRate = this.SPACE_ANIMATION_RATE;
      }
               
      sprite.exploding = true;

      this.explosionAnimator.start(sprite, true); 
     
   },

  // ********************** Animation ********************** 

   animate: function (now) { 
      if (spaceface.paused) {
         setTimeout( function () {
            requestNextAnimationFrame(spaceface.animate);
         }, spaceface.PAUSED_CHECK_INTERVAL);
      }
      else {
         spaceface.fps = spaceface.calculateFps(now); 
         spaceface.draw(now);
         requestNextAnimationFrame(spaceface.animate);
      }
   },

   togglePausedStateOfAllBehaviors: function () {
      var behavior;
   
      for (var i=0; i < this.sprites.length; ++i) { 
         sprite = this.sprites[i];

         for (var j=0; j < sprite.behaviors.length; ++j) { 
            behavior = sprite.behaviors[j];

            if (this.paused) {
               if (behavior.pause) {
                  behavior.pause(sprite);
               }
            }
            else {
               if (behavior.unpause) {
                  behavior.unpause(sprite);
               }
            }
         }
      }
   },

   togglePaused: function () {
      var now = +new Date();

      this.paused = !this.paused;
      this.togglePausedStateOfAllBehaviors();
   
      if (this.paused) {
         this.pauseStartTime = now;
      }
      else {
         this.lastAnimationFrameTime += (now - this.pauseStartTime);
      }
   },

   // ********************** INITIALIZATION ********************** 

   start: function () {
      this.createSprites();
      this.initializeImages();
      this.equipSpace();
      this.splashToast('Good Luck!');

      document.getElementById('instructions').style.opacity =
         spaceface.INSTRUCTIONS_OPACITY;
   },
   
   initializeImages: function () {
      var self = this;

      this.background.src = 'images/canvasBackground.jpg';
      this.spritesheet.src = 'images/spritesheet.png';
   
      this.background.onload = function (e) {
         self.startGame();
      };
   },

   startGame: function () {
      requestNextAnimationFrame(this.animate);
   },

   positionSprites: function (sprites, spriteData) {
      var sprite;

      for (var i = 0; i < sprites.length; ++i) {
         sprite = sprites[i];



            sprite.top  = spriteData[i].top;
            sprite.left = spriteData[i].left;
      }
   },


   
   addSpritesToSpriteArray: function () {
      for (var i=0; i < this.platforms.length; ++i) {
         this.sprites.push(this.platforms[i]);
      }

     for (var i=0; i < this.pTwos.length; ++i) {
         this.sprites.push(this.pTwos[i]);
      }

      for (var i=0; i < this.pOne.length; ++i) {
         this.sprites.push(this.pOne[i]);
      }

      for (var i=0; i < this.pThree.length; ++i) {
         this.sprites.push(this.pThree[i]);
      }

      for (var i=0; i < this.pFour.length; ++i) {
         this.sprites.push(this.pFour[i]);
      }
     
      for (var i=0; i < this.satalites.length; ++i) {
         this.sprites.push(this.satalites[i]);
      }


      for (var i=0; i < this.rocks.length; ++i) {
         this.sprites.push(this.rocks[i]);
      }

   },

   

    createSatalitesSprites: function () {
      var sat,
          satArtist = new SpriteSheetArtist(this.spritesheet, this.sataliteCells);

      for (var i = 0; i < this.sataliteData.length; ++i) {
         sat = new Sprite('satalite', satArtist);

         sat.width = this.SATALITE_CELLS_WIDTH;
         sat.height = this.SATALITE_CELLS_HEIGHT;

         this.satalites.push(sat);
      }
   },
   
        createPTwoSprites: function () {
      var two,
          twoArtist = new SpriteSheetArtist(this.spritesheet, this.twoCells);
   
      for (var i = 0; i < this.twoData.length; ++i) {
            two = new Sprite('two', twoArtist);

         two.width = this.TWO_CELLS_WIDTH;
         two.height = this.TWO_CELLS_HEIGHT;

         this.pTwos.push(two);
      }
   },
   
   createPOneSprites: function () {
      var one,
          oneArtist = new SpriteSheetArtist(this.spritesheet, this.pOneCells);
   
      for (var i = 0; i < this.pOneData.length; ++i) {
         one = new Sprite('one', oneArtist);

         one.width = this.PLANETONE_CELLS_WIDTH;
         one.height = this.PLANETONE_CELLS_HEIGHT;

         this.pOne.push(one);
      }
   },
   
   createPFourSprites: function () {
      var four,
          fourArtist = new SpriteSheetArtist(this.spritesheet, this.fourCells);
   
      for (var i = 0; i < this.fourData.length; ++i) {
         four = new Sprite('four', fourArtist);

         four.width = this.FOUR_CELLS_WIDTH;
         four.height = this.FOUR_CELLS_HEIGHT;

         this.pFour.push(four);
      }
   },
   
      createPThreeSprites: function () {
      var three,
          threeArtist = new SpriteSheetArtist(this.spritesheet, this.threeCells);
   
      for (var i = 0; i < this.threeData.length; ++i) {
         three = new Sprite('three', threeArtist);

         three.width = this.THREE_CELLS_WIDTH;
         three.height = this.THREE_CELLS_HEIGHT;
         
         this.pThree.push(three);
      }
   },

   createRocksSprites: function () {
      var rock,
          rockArtist = new SpriteSheetArtist(this.spritesheet, this.rockCells);
   
      for (var i = 0; i < this.rockData.length; ++i) {
         rock = new Sprite('rock', rockArtist,
                               [ new CycleBehavior(this.ROCK_SPARKLE_DURATION,
                                           this.ROCK_SPARKLE_INTERVAL),

                                 new BounceBehavior()
                               ]);

         rock.width = this.SPACEROCK_CELLS_WIDTH;
         rock.height = this.SPACEROCK_CELLS_HEIGHT;

         this.rocks.push(rock);
      }
   },

   
   updateSprites: function (now) {
      var sprite;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if (sprite.visible && this.spriteInView(sprite)) {
            sprite.update(now, this.fps, this.context);
         }
      }
   },
   
   drawSprites: function() {
      var sprite;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if (sprite.visible && this.spriteInView(sprite)) {
            this.context.translate(-sprite.offset, 0);

            sprite.draw(this.context);

            this.context.translate(sprite.offset, 0);
         }
      }
   },
   
   spriteInView: function(sprite) {
      return sprite === this.space || // space is always visible
         (sprite.left + sprite.width > this.spriteOffset &&
          sprite.left < this.spriteOffset + this.canvas.width);   
   },

   isOverPlatform: function (sprite, track) {
      var p,
          index = -1,
          center = sprite.left + sprite.offset + sprite.width/2;

      if (track === undefined) { 
         track = sprite.track; // Look on sprite track only
      }

      for (var i=0; i < spaceface.platforms.length; ++i) {
         p = spaceface.platforms[i];

         if (track === p.track) {
            if (center > p.left - p.offset && center < (p.left - p.offset + p.width)) {
               index = i;
               break;
            }
         }
      }
      return index;
   },
   

   
   createSprites: function() {  
     
      
    
      this.createSatalitesSprites();
      this.createPTwoSprites();
      this.createPOneSprites();
      this.createPThreeSprites();
      this.createPFourSprites();
      this.createRocksSprites();

      this.initializeSprites();

      this.addSpritesToSpriteArray();
   },
   
   initializeSprites: function() {  
      for (var i=0; i < spaceface.sprites.length; ++i) { 
         spaceface.sprites[i].offset = 0;
      }

      
      this.positionSprites(this.satalites,       this.sataliteData);
      this.positionSprites(this.pTwos,    this.twoData);
      this.positionSprites(this.pOne,      this.pOneData);
      this.positionSprites(this.pThree,     this.threeData);
      this.positionSprites(this.pFour,  this.fourData);
      this.positionSprites(this.rocks,  this.rockData);


   },

  // ********************** Toast ********************** 

   splashToast: function (text, howLong) {
      howLong = howLong || this.DEFAULT_TOAST_TIME;

      toast.style.display = 'block';
      toast.innerHTML = text;

      setTimeout( function (e) {
         if (spaceface.windowHasFocus) {
            toast.style.opacity = 1.0; // After toast is displayed
         }
      }, 50);

      setTimeout( function (e) {
         if (spaceface.windowHasFocus) {
            toast.style.opacity = 0; // Starts CSS3 transition
         }

         setTimeout( function (e) { 
            if (spaceface.windowHasFocus) {
               toast.style.display = 'none'; 
            }
         }, 480);
      }, howLong);
   },
};
   
// ********************** Event handlers ********************** 
   
window.onkeydown = function (e) {
   var key = e.keyCode;

   if (key === 80 || (spaceface.paused && key !== 80)) {  // 'p'
      spaceface.togglePaused();
   }

   else if (key === 75 || key === 39) { // 'k' or right arrow
      spaceface.turnRight();

   }
   else if (key === 74 || key === 38) { // 'j'
      spaceface.space.jump();

   }
   else if (key === 70 || key === 40) { // 'f'
      spaceface.space.fall();
   }
};

window.onblur = function (e) {  // pause if unpaused
   spaceface.windowHasFocus = false;
   
   if (!spaceface.paused) {
      spaceface.togglePaused();
   }
};

window.onfocus = function (e) {  // unpause if paused
   var originalFont = spaceface.toast.style.fontSize;

   spaceface.windowHasFocus = true;

   if (spaceface.paused) {
      spaceface.toast.style.font = '128px fantasy';

      spaceface.splashToast('3', 500); // Display 3 for one half second

      setTimeout(function (e) {
         spaceface.splashToast('2', 500); // Display 2 for one half second

         setTimeout(function (e) {
            spaceface.splashToast('1', 500); // Display 1 for one half second

            setTimeout(function (e) {
               if ( spaceface.windowHasFocus) {
                  spaceface.togglePaused();
               }

               setTimeout(function (e) { 
                  spaceface.toast.style.fontSize = originalFont;
               }, 2000);
            }, 1000);
         }, 1000);
      }, 1000);
   }
};

// ********************** Launch game ********************** 

var spaceface = new Spaceface();
spaceface.start();
